<?php return [
    'kit.securerandom' => ['App\DependencyLoader', 'loadSecureRandom'],
    'database' => ['App\DependencyLoader', 'loadDatabase'],
] + (require __DIR__ . '/config.php') + (require __DIR__ . '/database.php');