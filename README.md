# Sample Braid Application #

This project provides a simple application that provides structure for a
standard web application written using [Braid Framework](https://bitbucket.org/Riimu/braid-framework).

This is not intended as a serious project. Use at your own peril.

## Four Rules of Separation ##

  1. Only the repository is allowed access to the PDO instance
  2. A controller must never access the repository
  3. Each repository must be loaded through the dependency container
  4. Only the controller has the access to the dependency container
  
Addendum:

  - Record can only be accessed by the repository and the model that owns it

## Credits ##

This library is copyright 2015 to Riikka Kalliomäki.

See LICENSE for license and copying information.
