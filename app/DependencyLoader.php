<?php

namespace App;
use Riimu\Braid\Application\Container;
use Riimu\Kit\SecureRandom\SecureRandom;

/**
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2015, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
class DependencyLoader
{
    public static function loadSecureRandom()
    {
        return new SecureRandom();
    }

    public static function loadDatabase(Container $container)
    {
        $db = new \PDO(
            sprintf(
                "mysql:dbname=%s;host=%s;charset=utf8",
                $container->get('database.name'),
                $container->get('database.host')
            ),
            $container->get('database.user'),
            $container->get('database.pass'),
            [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone = '" . date('P') . "'",
            ]
        );

        $db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

        return $db;
    }
}
